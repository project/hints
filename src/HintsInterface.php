<?php

namespace Drupal\hints;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Hints entity.
 *
 * We have this interface so we can join the other interfaces it extends.
 *
 * @ingroup hints
 */
interface HintsInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
